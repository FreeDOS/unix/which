# which

Enables users to find executable files in directories listed in the PATH environment variable. Full wildcard support, relative pathnames, optional file details (size, date). User-definable program extensions, codepage-correct time/date formatting. New DOS switchar support.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## WHICH.LSM

<table>
<tr><td>title</td><td>which</td></tr>
<tr><td>version</td><td>2.1a</td></tr>
<tr><td>entered&nbsp;date</td><td>2002-03-20</td></tr>
<tr><td>description</td><td>Find executables located in the PATH directory list</td></tr>
<tr><td>keywords</td><td>DOS, extension, file, utility, find, locate, PATH</td></tr>
<tr><td>author</td><td>trane@gol.com (Trane Francks)</td></tr>
<tr><td>maintained&nbsp;by</td><td>trane@gol.com (Trane Francks)</td></tr>
<tr><td>primary&nbsp;site</td><td>http://www2.gol.com/users/trane/programming/programming.html</td></tr>
<tr><td>29kbwhich21.zip</td><td>29kBwhich21.zip</td></tr>
<tr><td>1kbwhich21.lsm</td><td>1kBwhich21.lsm</td></tr>
<tr><td>platforms</td><td>DOS, Compiler: Borland Turbo Pascal 7.0</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>summary</td><td>Enables users to find executable files in directories listed in the PATH environment variable. Full wildcard support, relative pathnames, optional file details (size, date). User-definable program extensions, codepage-correct time/date formatting. New DOS switchar support.</td></tr>
</table>
